#include <iostream>
#include <string>

using namespace std;

int translate(string line, string argument)
{
	int count = 0; // implementeer dit
	for(unsigned int i = 0; i < line.size(); i++){
		for(unsigned int j = 0; j < argument.size(); j++)
		{

  			if(line[i] == argument[j])
			{
        		count++;
        		}
		}
	}
	return count;
}

int main(int argc, char *argv[])
{ string line;

  if(argc != 2)
  { cerr << "Deze functie heeft exact 1 argument nodig" << endl;
    return -1; }

  while(getline(cin, line))
  { if(line == "EOF"){
	break;
  }
  cout << translate(line, argv[1]) << endl; }

  return 0; }
