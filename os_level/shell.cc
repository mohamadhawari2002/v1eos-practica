#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand
  std::string prompt;
  int fd = syscall(SYS_open, "prompt.txt", O_RDONLY, 0755);
  char byte[1];
  while(syscall(SYS_read, fd, byte, 1)){
 	prompt.append(byte);
	}
  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{

 std::string naam;
 std::cout << "De naam van het bestand: " << std::endl;
 std::getline(std::cin, naam);
 const char* filename = naam.c_str();
 int fd = syscall(SYS_creat, filename, 0755);
 syscall(SYS_open, filename, O_WRONLY, 0755);
 std::string tmp;
 std::cout << "voer uw tekst in: ";
 
 while(std::getline(std::cin, tmp)){
 	if(tmp == "<EOF>"){
 		break;
 	}
	tmp += "\n";
 	for(unsigned int i = 0; i < tmp.size(); i++){
		char byte[1] = {tmp[i]}; 
		syscall(SYS_write, fd, byte, 1);
	}


 } 
 syscall(SYS_close, fd);
}

void list() 
{
 int pid = syscall(SYS_fork);
 if(pid == 0){
	const char* args[] = { "/bin/ls", "-a", "-l", NULL};
	syscall(SYS_execve, args[0], args, NULL);
 
 }else{
	 syscall(SYS_wait4, pid, NULL, NULL, NULL);
 }

}

void find() // ToDo: Implementeer volgens specificatie.
{
 int fd[2];
 syscall(SYS_pipe, &fd);
 std::string sleutelwoord;
 std::cout << "op welke woord wilt uw zoeken: " << std::endl;
 std::getline(std::cin, sleutelwoord);
 int pid = syscall(SYS_fork);
 if(pid == 0){
 	syscall(SYS_close, fd[0]);
        syscall(SYS_dup2, fd[1], 1);
        const char* args[] = {"/usr/bin/find", ".", NULL};
        syscall(SYS_execve, args[0], args, NULL);


 }else{
	int pid1 = syscall(SYS_fork);
	if(pid1 == 0){
                syscall(SYS_close, fd[1]);
                syscall(SYS_dup2, fd[0], 0);
                const char* args1[] = {"/bin/grep", sleutelwoord.c_str(), NULL};
                syscall(SYS_execve, args1[0], args1, NULL);
	}else{
	syscall(SYS_close, fd[0]);
	syscall(SYS_close, fd[1]);	
 	syscall(SYS_wait4, pid, NULL, NULL, NULL);
	syscall(SYS_wait4, pid1, NULL, NULL, NULL);
	}
 }
}


void seek() // ToDo: Implementeer volgens specificatie.
{
 
 int seek = syscall(SYS_creat, "seek", 0644);
 
 char x[1] = {'x'};

 syscall(SYS_write, seek, x, 1);
 
 syscall(SYS_lseek, seek, 5000000, 1);

 syscall(SYS_write, seek, x, 1);
 
 char null[1] = {'\0'};

 int loop = syscall(SYS_creat, "loop", 0644);
 
 syscall(SYS_write, loop, x, 1);
 
 for(int i=0; i < 5000000; i++){
	syscall(SYS_write, loop, null, 1);
 }

 syscall(SYS_write, loop, x, 1);

}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1)){                  // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte;}                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
}
